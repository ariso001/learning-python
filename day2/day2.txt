so guys 
i want a software i can call with :

exercise2.py man 
or 
exercise2.py girl 

what i want:


How old are you?  38
How tall are you?  118cm
How much do you weigh?  180kg
So, you're '3894' weeks old, '6\'2"' tall and '180lbs' heavy.
basically it have to convert the parameter (kg to lbs and cm to feet year to weeks) 
it have to take input man or girl and save the result in a file 
result man.txt 
result girl.txt 
if it's called more than 1 time it have to append to result to the same file 


resource;

http://learnpythonthehardway.org/book/ex13.html
https://docs.python.org/2/tutorial/inputoutput.html
https://stackoverflow.com/questions/3345202/python-getting-user-input
https://docs.python.org/2/library/math.html
